﻿using NetMQ;
using NetMQ.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Threading;


namespace MockService
{
    class Open_Sequence
    {
        public string Route { get; set; }
        public string WorkSpace { get; set; }
        public string project { get; set; }
    }

    class Program
    {
        //the script that will return the number of workspaces, uppon this a workspace will be selected 
        static String WorkSpaceSearchScript = String.Concat("version \"5.0\"", "\n",
                                        "DECLARE wscount", "\n",
                                        "WORKSPACE COUNT GET(wscount)", "\n");
        static String WorkSpaceNameGetScript = String.Concat("version \"5.0\"", "\n",
                                        "DECLARE wsname", "\n",
                                        "WORKSPACE PROPERTIES FILENAME GET ( $1, wsname )", "\n");
        static String WorkSpaceOpenWSScript = String.Concat("version \"5.0\"", "\n",
                                        "FILE OPEN_WORKSPACE ( $1 )");
        static String WorkSpaceGetProjectcountScript = String.Concat("version \"5.0\"", "\n",
                                                    "DECLARE pjcount", "\n",
                                                    "TREEVIEW POLYWORKS_INSPECTOR_PROJECT COUNT GET(pjcount)");
        static String WorkspaceGetProjectNameScript = String.Concat("version \"5.0\"", "\n",
                                                    "DECLARE pjname", "\n",
                                                    "TREEVIEW POLYWORKS_INSPECTOR_PROJECT NAME GET ( $1, pjname )");
        static String InspectorOpenProjectScript = String.Concat("version \"5.0\"", "\n", 
                                                    "FILE OPEN_PROJECT_IN_PWK($1, $2, )");
        static void Main(string[] args)
        {

            Open_Sequence sequence = new Open_Sequence();

            Console.WriteLine("creating proactor");
            //initialize the proactor that will receive the messages
            var receiveSocket = new SubscriberSocket("tcp://192.168.1.95:1234");
            receiveSocket.Subscribe("B");
            var proactor = new NetMQProactor(receiveSocket, (socket, message) => ProcessMessage(message, ref sequence));
            Console.WriteLine("listening on B");

            //Console.WriteLine("Hello World!");


        }
        private static List<string>  searchPWK(string sourceDirectory) {
            Console.WriteLine("db1");
            Console.WriteLine(sourceDirectory);
            List<string> pwkFiles = Directory.EnumerateFiles(sourceDirectory,
                                        "*", SearchOption.TopDirectoryOnly)
               .Where(s => s.EndsWith(".pwk") && s.Count(c => c == '.') < 2)
               .ToList();
            foreach (string str in pwkFiles)
            {
                Console.WriteLine(str);
            }
            Console.WriteLine("db2");
            return pwkFiles;
        }

        
        private static void ProcessMessage(NetMQMessage message, ref Open_Sequence sequence)
        {
            for (int i = 0; i < message.FrameCount; i++)
            {
                Console.Write(message[i].ConvertToString() + ", ");
            }
            Console.WriteLine();

            string proxyIP = "tcp://192.168.1.95:5678";
            //IM setup
            //IMEditLib.IMEdit imEditObject = new IMEditLib.IMEdit();
            IMInspectLib.IMInspect m_imInspect = null;
            IMInspectLib.IIMCommandCenter imInspectCommandCenter = null;
            IMInspectLib.IIMInspectProject imInspectProject = null;
            // Create a COM Message Filter to prevent COM calls from being rejected if the server application is temporarily busy
            //CRAIIComMessageFilter comMsgFilter = new CRAIIComMessageFilter();
            int returnValue;
            long cmdExecRes = 0;
            // Start the Workspace Manager
            IMWorkspaceManagerLib.IIMWorkspaceManager2 workspaceManager2 = new IMWorkspaceManagerLib.IMWorkspaceManager() as IMWorkspaceManagerLib.IIMWorkspaceManager2;
            if (workspaceManager2 == null)
            {
                return;
            }
            m_imInspect = new IMInspectLib.IMInspect();
            if (m_imInspect == null)
            {
                return;
            }

            // Get the Workspace Manager command center
            IMWorkspaceManagerLib.IIMCommandCenter wmCommandCenter = null;
            workspaceManager2.CommandCenterCreate(out wmCommandCenter);
            if (wmCommandCenter == null)
            {
                return;
            }
            //if (imEditObject == null)
            //{
            //    return;
            //}
            m_imInspect.ProjectGetCurrent(out imInspectProject);
            imInspectProject.CommandCenterCreate(out imInspectCommandCenter);



            /*discriminate comand
             *commands: search, open request
             * if search: use the interop from IM to look for projects in the default project folder
             *            use the user name to search for PWK file extension
             *            use the command: FILE OPEN_WORKSPACE ( "C:\Users\DiegoPantoja\IN-010_FuelDoor_completed.pwk" ) to open all 
             *            generate a message object
             *            append the project names
             *            send the object
             *            publish multiple times?
             * if open request
             *            use the string given to select the project and open it
             */

            if (message[1].ConvertToString() == "searchws")
            {
                Console.WriteLine("received a request for WS search");
                string pwkDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\PolyWorks";
                Console.WriteLine(pwkDir);
                Console.WriteLine("searching for pwks");
                List<string> pwkFiles = new List<string>();
                pwkFiles = searchPWK(pwkDir);
                foreach (string str in pwkFiles) Console.WriteLine(str);

                //publish the workspaces
                using (var pub = new PublisherSocket())
                {
                    pub.Connect(string.Format(proxyIP)); // Use connect when there is a proxy
                                                         //pub.Bind(string.Format("tcp://192.168.1.90:5678")); // Use bind when there is no proxy
                    NetMQMessage myMessage = new NetMQMessage();
                    myMessage.Append("B");
                    myMessage.Append("searchresult");
                    foreach (String ls in pwkFiles)
                    {
                        myMessage.Append(ls);
                    }
                    Thread.Sleep(1000);
                    pub.SendMultipartMessage(myMessage);
                }

            } else if (message[1].ConvertToString() == "openworkspacerequest")
            {
                //todo save the workspace on a DTO oibject rather than a local var
                string m_workspace = message[2].ConvertToString();
                sequence.WorkSpace = m_workspace;
                int m_projectcount = 0;
                string m_pjname = null;

                //Execute a command on the Workspace Manager
                List<string> m_projects = new List<string>();
                //cmdExecRes = wmCommandCenter.CommandExecute("MACRO PAUSE( \"Message\", \"Workspace Manager started\" )");
                cmdExecRes = wmCommandCenter.ScriptExecuteFromBuffer(WorkSpaceOpenWSScript, m_workspace);
                cmdExecRes = wmCommandCenter.ScriptExecuteFromBuffer(WorkSpaceGetProjectcountScript, "");
                wmCommandCenter.ScriptVariableGetValueAsInt("pjcount", 1, out m_projectcount);
                if (m_projectcount == 1)
                {
                    wmCommandCenter.ScriptExecuteFromBuffer(WorkspaceGetProjectNameScript, "");
                    wmCommandCenter.ScriptVariableGetValueAsString("pjname", 1, out m_pjname);
                    using (var pub = new PublisherSocket())
                    {
                        pub.Connect(string.Format(proxyIP)); // Use connect when there is a proxy
                        //pub.Bind(string.Format("tcp://192.168.1.90:5678")); // Use bind when there is no proxy
                        NetMQMessage myMessage = new NetMQMessage();
                        myMessage.Append("B");
                        myMessage.Append("openworkspacestatus");
                        myMessage.Append(m_pjname);
                        Thread.Sleep(1000);
                        pub.SendMultipartMessage(myMessage);
                    }
                } else
                {
                    List<string> myProjects = new List<string>();
                    for (int i = 1; i <= m_projectcount; i++)
                    {
                        wmCommandCenter.ScriptExecuteFromBuffer(WorkspaceGetProjectNameScript, i.ToString());
                        wmCommandCenter.ScriptVariableGetValueAsString("pjname", 1, out m_pjname);
                        myProjects.Add(m_pjname);
                        Console.WriteLine(m_pjname);
                    }
                    using (var pub = new PublisherSocket())
                    {
                        pub.Connect(string.Format(proxyIP)); // Use connect when there is a proxy
                                                             //pub.Bind(string.Format("tcp://192.168.1.90:5678")); // Use bind when there is no proxy
                        NetMQMessage myMessage = new NetMQMessage();
                        myMessage.Append("B");
                        myMessage.Append("openworkspacestatus");
                        foreach (String ls in myProjects)
                        {
                            myMessage.Append(ls);
                        }
                        Thread.Sleep(1000);
                        pub.SendMultipartMessage(myMessage);
                    }
                }
            } else if (message[1].ConvertToString() == "openprojectrequest")
            {
                sequence.project = message[2].ConvertToString();
                //recieve a project name and open such project using the project name and workspace fileroute as params
                String scriptparams = String.Concat(sequence.WorkSpace, " ", sequence.project);
                returnValue = imInspectCommandCenter.ScriptExecuteFromBuffer(InspectorOpenProjectScript, scriptparams);
                if (imInspectCommandCenter.ReturnValueIsSuccess(returnValue) == 1)
                {
                    using (var pub = new PublisherSocket())
                    {
                        pub.Connect(string.Format(proxyIP)); // Use connect when there is a proxy
                        //pub.Bind(string.Format("tcp://192.168.1.90:5678")); // Use bind when there is no proxy
                        NetMQMessage myMessage = new NetMQMessage();
                        myMessage.Append("B");
                        myMessage.Append("openprojectstatus");
                        myMessage.Append("S_OK");
                        Thread.Sleep(1000);
                        pub.SendMultipartMessage(myMessage);
                    }
                }
                else
                {
                    using (var pub = new PublisherSocket())
                    {
                        pub.Connect(string.Format(proxyIP)); // Use connect when there is a proxy
                        //pub.Bind(string.Format("tcp://192.168.1.90:5678")); // Use bind when there is no proxy
                        NetMQMessage myMessage = new NetMQMessage();
                        myMessage.Append("B");
                        myMessage.Append("openprojectstatus");
                        myMessage.Append("S_NOTOK");
                        Thread.Sleep(1000);
                        pub.SendMultipartMessage(myMessage);
                    }
                }
            }

            // Release the Workspace Manager
            // This last release should also close the Workspace Manager
            System.Runtime.InteropServices.Marshal.ReleaseComObject(workspaceManager2);
            workspaceManager2 = null;
        }
    }
}
